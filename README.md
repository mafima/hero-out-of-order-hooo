# HOOO - README #

### SUMMARY ###


* fast and simple online 1v1 hero arena
* created by: Florian Marx, Manuel Fischer
* game language english / german
* [link placeholder](http://ericskiff.com/music/)

## ✔ finished ✔ ##

* readme
* hit sound
* End button working
* START menu
* phase 1: matchmaking
* phase 2: start game
* main menu sound for buttons
* bg music
* Gamemode 2: OSU style - circles that move randomly on screen on click
* counter: 3,2,1

### TODO ###

* basic hero system
* health variable
* counter: 3,2,1
* optional: boni
    1. random circle > by click +10 points
    2. Rocket > by click move to enemy + 5 points
* low: anticheat:
clickbot 1: constant flag
clickbot 2: too fast flag
* OPTIONS (music/graphics/colors)
* CREDITS

### HEROES ###

#new:
* pitbull style char (die antwoord)
* mystic girl ghost
* more ORIGINAL unique characters!

#adaptement plans:
* supa hot fire

#canceled:
* generec heroes