﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Transform youText;

	// Use this for initialization
	void Start () {
	    // spawn player object
        if(PhotonNetwork.isMasterClient)
        {
            // spawn player 1
            GameObject p1_spawn = GameObject.Find("p1_spawn");

            GameObject player1 = PhotonNetwork.Instantiate("player", p1_spawn.transform.position, Quaternion.identity, 0);
            player1.name = "Player 1";
            player1.GetComponent<Player>().setID(1);

            Transform transform = (Transform)Instantiate(youText, new Vector3(player1.transform.position.x, player1.transform.position.y - 100), Quaternion.identity);
            transform.SetParent(GameObject.Find("Canvas").transform);
        }
        else
        {
            // spawn player 2
            GameObject p2_spawn = GameObject.Find("p2_spawn");

            GameObject player2 = PhotonNetwork.Instantiate("player", p2_spawn.transform.position, Quaternion.identity, 0);
            player2.name = "Player 2";
            player2.transform.FindChild("Button Layer").GetComponent<Image>().color = new Color32(200, 0, 30, 255);
            player2.GetComponent<Player>().setID(2);

            Transform transform = (Transform)Instantiate(youText, new Vector3(player2.transform.position.x, player2.transform.position.y - 100), Quaternion.identity);
            transform.SetParent(GameObject.Find("Canvas").transform);
        }
	}
}
