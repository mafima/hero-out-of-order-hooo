﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Player : MonoBehaviour {

	private Transform moveme;

	public int playerID;

	// Use this for initialization
	void Start () {
		moveme = GameObject.Find("moveme").transform;
		this.transform.SetParent(GameObject.Find("Canvas").transform);

		Button button = GetComponentInChildren<Button>();
		button.onClick.AddListener(delegate() { onPlayerButtonClick(); });
	}

    public void setID(int playerID)
    {
        this.playerID = playerID;
    }

	public void onPlayerButtonClick() {
		if(GetComponent<PhotonView>().isMine) {
            //GetComponentInChildren<Image>().color = new Color(255,50,100);
            GetComponent<PhotonView>().RPC("CmdaddPointTo", PhotonTargets.MasterClient, new object[] { playerID });
			//CmdaddPointTo(playerID);
		}
	}

	//[Command]
    [PunRPC]
	public void CmdaddPointTo(int playerID) {
		Vector3 pos = moveme.position;
		if(playerID == 1) pos.x += Screen.width * 0.01f;
		else pos.x -= Screen.width * 0.01f;
		moveme.position = pos;

		Debug.Log(moveme.localPosition.x);

		checkWin();
	}

	//[Server]
	public void checkWin() {
		if(moveme.localPosition.x <= -520) {
			// player 2 wins
			Debug.Log("Player 2 wins!");
		} else if(moveme.localPosition.x >= 520) {
			// player 1 wins
			Debug.Log("Player 1 wins!");
		}
	}
}
