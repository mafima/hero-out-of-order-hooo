﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class SimplePrompt : MonoBehaviour {

    public static GameObject GetGameObject()
    {
        return GameObject.Find("PromptWindow");
    }

    public static Transform GetTransform()
    {
        if(Exists())
        {
            return GameObject.Find("PromptWindow").transform;
        }
        return null;
    }

    public static bool Exists()
    {
        return GameObject.Find("PromptWindow") != null;
    }

    public static bool SpawnWithExistingContent(string title, Transform content, string positive = "POSITIVE", UnityAction onPositiveListener = null, string negative = "NEGATIVE", UnityAction onNegativeListener = null, bool exitButtonEnabled = true)
    {
        if (Exists()) return false;

        // spawn window
        Transform promptWindow = (Instantiate(Resources.Load("Prefabs/PromptWindow")) as GameObject).transform;
        promptWindow.name = "PromptWindow";
        //promptWindow.SetParent(GameObject.Find("Canvas").transform);

        promptWindow.localPosition = new Vector3();

        // set content
        if (content != null)
        {
            content.SetParent(promptWindow.Find("PanelLayer").Find("contentPanel"));
            content.localPosition = content.position;
        }

        // set title
        promptWindow.Find("PanelLayer").Find("titlePanel").Find("titleText").GetComponent<Text>().text = title;

        // set positive text
        if (string.IsNullOrEmpty(positive))
        {
            GameObject.Destroy(promptWindow.Find("PanelLayer").Find("positiveButton").gameObject);
        }
        else
        {
            promptWindow.Find("PanelLayer").Find("positiveButton").Find("Button Layer").Find("Button Text").GetComponent<Text>().text = positive;
        }

        // set negative text
        if (string.IsNullOrEmpty(negative))
        {
            GameObject.Destroy(promptWindow.Find("PanelLayer").Find("negativeButton").gameObject);
        }
        else
        {
            promptWindow.Find("PanelLayer").Find("negativeButton").Find("Button Layer").Find("Button Text").GetComponent<Text>().text = negative;
        }

        if (exitButtonEnabled)
        {
            promptWindow.Find("PanelLayer").Find("titlePanel").Find("exitButton").Find("Button Layer").GetComponent<Button>().interactable = true;
        }

        promptWindow.Find("PanelLayer").Find("titlePanel").Find("exitButton").Find("Button Layer").GetComponent<Button>().onClick.AddListener(delegate () { Remove(); });

        SetOnPositiveListener(onPositiveListener);
        SetOnNegativeListener(onNegativeListener);

        promptWindow.SetParent(GameObject.Find("Canvas").transform);
        promptWindow.localPosition = promptWindow.position;

        return true;
    }

    public static bool Spawn(string title, Transform contentPrefab, string positive = "POSITIVE", UnityAction onPositiveListener = null, string negative = "NEGATIVE", UnityAction onNegativeListener = null, bool exitButtonEnabled = true)
    {
        return Spawn(title, (Instantiate(contentPrefab) as Transform), positive, onPositiveListener, negative, onNegativeListener, exitButtonEnabled);
    }

    public static bool Remove()
    {
        if (!Exists()) return false;

        GameObject.Destroy(GameObject.Find("PromptWindow"));

        return true;
    }
    

    private static void SetOnPositiveListener(UnityAction listener)
    {
        if(Exists())
        {
            Transform positiveButton = GetTransform().Find("PanelLayer").Find("positiveButton").Find("Button Layer");
            if(positiveButton != null)
            {
                positiveButton.GetComponent<Button>().onClick.AddListener(listener);
            }
        }
    }

    private static void SetOnNegativeListener(UnityAction listener)
    {
        if (Exists())
        {
            Transform negativeButton = GetTransform().Find("PanelLayer").Find("negativeButton").Find("Button Layer"); ;
            if (negativeButton != null)
            {
                negativeButton.GetComponent<Button>().onClick.AddListener(listener);
            }
        }
    }

}
