﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour {

    const string VERSION = "v0.0.1";

	// Use this for initialization
	void Start () {
        PhotonNetwork.ConnectUsingSettings(VERSION);
        PhotonNetwork.automaticallySyncScene = true;
    }

    void OnConnectedToMaster()
    {
        Button button = GameObject.Find("start").transform.FindChild("Button Layer").GetComponent<Button>();
        button.interactable = true;
    }

    public static void StartMatchmaking()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        RoomOptions roomOptions = new RoomOptions()
        {
            isVisible = true,
            maxPlayers = 2
        };
        PhotonNetwork.CreateRoom(Guid.NewGuid().ToString(), roomOptions, TypedLobby.Default);
        //PhotonNetwork.LoadLevel("Game2");
    }

    void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if(PhotonNetwork.playerList.Length == 2)
        {
            // start game
            PhotonNetwork.LoadLevel("Game2");
        }
    }

}
