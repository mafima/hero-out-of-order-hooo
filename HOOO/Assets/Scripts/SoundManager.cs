﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SoundManager : MonoBehaviour {

    private static SoundSettings soundSettings;

    public static void setMusicVolume(float musicVolume)
    {
        if(soundSettings == null)
        {
            soundSettings = new SoundSettings();
        }
        soundSettings.musicVolume = musicVolume;
    }
    public static float getMusicVolume()
    {
        if(soundSettings == null)
        {
            Load();
        }
        return soundSettings.musicVolume;
    }

    public static void setSoundValue(float soundVolume)
    {
        if (soundSettings == null)
        {
            soundSettings = new SoundSettings();
        }
        soundSettings.soundVolume = soundVolume;
    }
    public static float getSoundVolume()
    {
        if (soundSettings == null)
        {
            Load();
        }
        return soundSettings.soundVolume;
    }

    public static void Save()
    {
        if (!Directory.Exists("Saves")) Directory.CreateDirectory("Saves");

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Create("Saves/save.binary");

        formatter.Serialize(saveFile, soundSettings);

        saveFile.Close();
    }

    public static void Load()
    {
        if(!Directory.Exists("Saves"))
        {
            soundSettings = new SoundSettings();
            Save();
            return;
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Open("Saves/save.binary", FileMode.Open);

        soundSettings = (SoundSettings)formatter.Deserialize(saveFile);

        saveFile.Close();
    }

}

[Serializable]
class SoundSettings
{
    //public float musicVolume = 0.3f;
    //public float soundVolume = 0.5f;
    public float musicVolume = 1f;
    public float soundVolume = 1f;
}
