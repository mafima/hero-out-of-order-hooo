﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        /*Transform text = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SimpleTextPrompt")) as GameObject).transform;
        text.GetComponent<Text>().text = "This is a little test screen!";

        SimplePrompt.SpawnWithExistingContent(
            "Welcome to MafiClicker",
            text,
            "Continue",
            delegate () { SimplePrompt.Remove(); },
            "Quit game",
            delegate () { Application.Quit(); },
            false);*/

        // change music volume
        foreach(AudioSource audioSource in transform.GetComponents<AudioSource>())
        {
            if(audioSource.clip.name.StartsWith("0"))
            {
                audioSource.volume = SoundManager.getMusicVolume();
            }
            else
            {
                audioSource.volume = SoundManager.getSoundVolume();
            }
        }
    }

	public void onStartClick() {
        AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
        audio.Play();
        // phase 1: matchmaking

        NetworkManager.StartMatchmaking();

        // disable button interaction
        Button button = GameObject.Find("start").transform.FindChild("Button Layer").GetComponent<Button>();
        button.interactable = false;

        GameObject panel = GameObject.Find("loading");
        panel.transform.Find("PanelLayer").GetComponent<Image>().enabled = true;
        StartCoroutine("rotateLoading", panel);

		// phase 2: load scene
		//SceneManager.LoadScene("Game");
	}

    public void onOptionsClick()
    {
        AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
        audio.Play();

        Transform settingsWindow = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SettingsPrompt")) as GameObject).transform;
        settingsWindow.Find("MusicVolume").GetComponent<Slider>().value = SoundManager.getMusicVolume();
        settingsWindow.Find("SoundVolume").GetComponent<Slider>().value = SoundManager.getSoundVolume();

        // show options screen
        SimplePrompt.SpawnWithExistingContent(
            "Options",
            settingsWindow,
            "Save",
            delegate ()
            {
                SoundManager.setMusicVolume(settingsWindow.Find("MusicVolume").GetComponent<Slider>().value);
                SoundManager.setSoundValue(settingsWindow.Find("SoundVolume").GetComponent<Slider>().value);
                SoundManager.Save();
                // change music volume
                foreach (AudioSource audioSource in transform.GetComponents<AudioSource>())
                {
                    if (audioSource.clip.name.StartsWith("0"))
                    {
                        audioSource.volume = SoundManager.getMusicVolume();
                    }
                    else
                    {
                        audioSource.volume = SoundManager.getSoundVolume();
                    }
                }
                SimplePrompt.Remove();
            },
            "Cancel",
            delegate ()
            {
                SimplePrompt.Remove();
            },
            true);
    }

    public void onCreditsClick()
    {
        AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
        audio.Play();

        Transform creditsContent = (Instantiate(Resources.Load("Prefabs/SimplePrompt/CreditsPrompt")) as GameObject).transform;
        SimplePrompt.SpawnWithExistingContent(
            "Credits",
            creditsContent,
            "",
            null,
            "",
            null,
            true);
    }

    IEnumerator rotateLoading(GameObject rotateObject)
    {
        while(true)
        {
            rotateObject.transform.Rotate(new Vector3(0, 0, -3));
            yield return new WaitForSeconds(0.005f);
        }
    }

    public void onEndClick () {
		Application.Quit();
	}
}
