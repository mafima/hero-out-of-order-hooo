using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManagerGM2 : MonoBehaviour {

    public Transform youText;
    public Transform clickButton;

    public static GameManagerGM2 instance;

    private Transform moveme;

    public AudioClip audioClick;
    public AudioClip audioWin;

    private static bool started = false;

    // Use this for initialization
    void Start () {
        instance = this;
        moveme = GameObject.Find("moveme").transform;
        started = true;

        if (PhotonNetwork.isMasterClient)
        {
            GameObject player1 = GameObject.Find("youTextP1Spawn");
            Transform transform = (Transform)Instantiate(youText, new Vector3(player1.transform.position.x, player1.transform.position.y), Quaternion.identity);
            transform.SetParent(GameObject.Find("Canvas").transform);
        }
        else
        {
            GameObject player2 = GameObject.Find("youTextP2Spawn");
            Transform transform = (Transform)Instantiate(youText, new Vector3(player2.transform.position.x, player2.transform.position.y), Quaternion.identity);
            transform.SetParent(GameObject.Find("Canvas").transform);
        }

        // change music volume
        foreach (AudioSource audioSource in transform.GetComponents<AudioSource>())
        {
            if (audioSource.clip.name.StartsWith("0"))
            {
                audioSource.volume = SoundManager.getMusicVolume();
            }
            else
            {
                audioSource.volume = SoundManager.getSoundVolume();
            }
        }

        if(!PhotonNetwork.isMasterClient)
        {
            GetComponent<PhotonView>().RPC("ClientReady", PhotonTargets.MasterClient, new object[] { });
        }

        //SpawnRandom(false);
	}

    public void removePoint()
    {
        GameManagerGM2.instance.GetComponent<PhotonView>().RPC("addPointTo", PhotonTargets.MasterClient, new object[] { (PhotonNetwork.isMasterClient) ? 2 : 1 });
    }

    public static void SpawnRandom(bool addPoints)
    {
        if(addPoints && started)
        {
            GameManagerGM2.instance.GetComponent<PhotonView>().RPC("addPointTo", PhotonTargets.MasterClient, new object[] { (PhotonNetwork.isMasterClient) ? 1 : 2 });
        }

        // remove old if exists
        GameObject buttonObject = GameObject.Find("clickButton");
        if(buttonObject != null)
        {
            Object.Destroy(buttonObject);
        }

        if(started)
        {
            // add new
            float minx = -508.5f, maxx = 508.5f, maxy = 180.5f, miny = -180.5f;

            float x = Random.Range(minx, maxx);
            float y = Random.Range(miny, maxy);

            Transform transform = (Transform)Instantiate(GameManagerGM2.instance.clickButton, new Vector3(), Quaternion.identity);
            transform.SetParent(GameObject.Find("circleSpawnPanel").transform);
            transform.localPosition = new Vector3(x, y);
        }
    }

    [PunRPC]
    public void ClientReady()
    {
        StartCoroutine("CountdownServer");
    }

    int countdown = 3;
    IEnumerator CountdownServer()
    {
        while (countdown >= 0)
        {
            GetComponent<PhotonView>().RPC("GameStart", PhotonTargets.All, new object[] { countdown });
            countdown--;
            yield return new WaitForSeconds(1f);
        }
        SimplePrompt.Remove();
    }

    [PunRPC]
    public void GameStart(int countdown)
    {
        if(countdown > 0)
        {
            if(SimplePrompt.Exists())
            {
                GameObject.Find("PromptWindow").transform.Find("PanelLayer").Find("contentPanel").Find("SimpleTextPrompt").GetComponent<Text>().text = "Game starting in\n" + countdown + "\nseconds!";
            }
            else
            {
                Transform text = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SimpleTextPrompt")) as GameObject).transform;
                text.name = "SimpleTextPrompt";
                text.GetComponent<Text>().text = "Game starting in\n" + countdown + "\nseconds!";

                SimplePrompt.SpawnWithExistingContent(
                "",
                text,
                "",
                null,
                "",
                null,
                false);
            }
        }
        else
        {
            // start game
            SimplePrompt.Remove();
            started = true;
            SpawnRandom(false);
        }
    }

    [PunRPC]
    public void addPointTo(int playerID)
    {
        if(started)
        {
            Vector3 pos = moveme.position;
            if (playerID == 1) pos.x += Screen.width * 0.05f;
            else pos.x -= Screen.width * 0.05f;
            moveme.position = pos;

            checkWin();
        }
    }

    [PunRPC]
    public void GameOver(int playerIDWon)
    {
        started = false;

        GameObject buttonObject = GameObject.Find("clickButton");
        if (buttonObject != null)
        {
            Object.Destroy(buttonObject);
        }
        GameObject.Find("circleSpawnPanel").transform.Find("PanelLayer").GetComponent<Button>().interactable = false;

        if (PhotonNetwork.isMasterClient && playerIDWon == 1)
        {
            Transform text = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SimpleTextPrompt")) as GameObject).transform;
            text.GetComponent<Text>().text = "YOU HAVE WON THE GAME! :)";
            SimplePrompt.SpawnWithExistingContent("Game over!", text, "", null, "Exit game", delegate() {
                PhotonNetwork.LeaveRoom();
                SceneManager.LoadScene("MainMenu");
            }, false);
        }
        else if(!PhotonNetwork.isMasterClient && playerIDWon == 2)
        {
            Transform text = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SimpleTextPrompt")) as GameObject).transform;
            text.GetComponent<Text>().text = "YOU HAVE WON THE GAME! :)";
            SimplePrompt.SpawnWithExistingContent("Game over!", text, "", null, "Exit game", delegate()
            {
                PhotonNetwork.LeaveRoom();
                SceneManager.LoadScene("MainMenu");
            }, false);
        }
        else
        {
            Transform text = (Instantiate(Resources.Load("Prefabs/SimplePrompt/SimpleTextPrompt")) as GameObject).transform;
            text.GetComponent<Text>().text = "YOU HAVE LOST THE GAME! :(";
            SimplePrompt.SpawnWithExistingContent("Game over!", text, "", null, "Exit game", delegate() {
                PhotonNetwork.LeaveRoom();
                SceneManager.LoadScene("MainMenu");
            }, false);
        }
    }

    private void checkWin()
    {
        if (moveme.localPosition.x <= -550)
        {
            GameManagerGM2.instance.GetComponent<PhotonView>().RPC("GameOver", PhotonTargets.All, new object[] { 2 });
            // player 2 wins
            Debug.Log("Player 2 wins!");

            AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
            audio.clip = audioWin;
            audio.Play();
        }
        else if (moveme.localPosition.x >= 550)
        {
            GameManagerGM2.instance.GetComponent<PhotonView>().RPC("GameOver", PhotonTargets.All, new object[] { 1 });

            // player 1 wins
            Debug.Log("Player 1 wins!");

            AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
            audio.clip = audioWin;
            audio.Play();
        }
    }
}
