﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonGM2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.name = "clickButton";
        Button button = GetComponentInChildren<Button>();
        button.onClick.AddListener(delegate () {
            AudioSource audio = GameObject.Find("GM").GetComponent<AudioSource>();
            audio.clip = GameObject.Find("GM").GetComponent<GameManagerGM2>().audioClick;
            audio.Play();

            GameManagerGM2.SpawnRandom(true);
        });
    }

}
